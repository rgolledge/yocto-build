FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
ENV LANG en_GB.UTF-8
ENV LC_ALL en_GB.UTF-8

RUN apt-get update -y \
 && apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common \
        build-essential \
        chrpath \
        cpio \
        debianutils \
        diffstat \
	bc \
        gawk \
        gcc-multilib \
        git-core \
        git-lfs \
        g++-multilib \
        libncurses-dev \
        texinfo \
        locales \
        python \
        python3 \
        python3-pip \
        python3-pexpect \
        socat \
        sudo \
        xz-utils \
        iputils-ping \
        libsdl1.2-dev \
        xterm \
        tar \
        neovim \
        zstd \
        tmux \
        rsync \
        unzip \
        wget \
        zstd \
	liblz4-tool \
	cmake \
        bison \
        build-essential \
        curl \
        flex \
        fontconfig \
        git-core \
        gnupg \
        lib32z1-dev \
        libc6-dev-i386 \
        libgl1-mesa-dev \
        libncurses5 \
        libssl-dev \
        libx11-dev \
        libxml2-utils \
        make \
        neovim \
        net-tools \
        python \
        unzip \
        x11proto-core-dev \
        xsltproc \
        zip \
        zlib1g-dev \
	kmod \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /tmp/* \
 && rm -rf /var/tmp/*

RUN rm /bin/sh && ln -s bash /bin/sh

RUN locale-gen en_US.UTF-8 en_GB.UTF-8 && update-locale LC_ALL=en_GB.UTF-8 \
    LANG=en_GB.UTF-8

# Repo is a useful tool for checking out multiple git repos
RUN wget https://storage.googleapis.com/git-repo-downloads/repo -O /usr/local/bin/repo && chmod +x /usr/local/bin/repo 
VOLUME /yocto/sstate


